# Ubuntu development environment

Install the following packages:
`apt-get install msbuild mono-complete mono-roslyn`

# To build it using mono and msbuild under Ubuntu
`cd SeclavePlugin && msbuild -p:Configuration=Release`
